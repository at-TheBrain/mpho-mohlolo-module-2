class AppDetails {
  //Declaring and initializing variables
  var appName = "mth";
  var developer = "James";
  var year = "19365";
  var category = "Health";

  void printFunction() {
    print(
        "application name : ${appName}  \nCategory: ${category} \nDeveloperName: ${developer} \nYear It won: ${year}");
  }

  //Function that capitizes the name
  String capitalize(String appName) {
    //declare a variable that will store the capitalized name of the app
    String appCaps = appName.toUpperCase();

    return appCaps;
  }
}

//Main method to run the aplication
void main() {
  //Create an object of the class

  AppDetails app = new AppDetails();

  //Invoke the print Method
  app.printFunction();

  //Invoke the caps method
//create a variable that will store the returned variables
  String appCaps = app.capitalize(app.appName);

  print("Poeple");
  print("The capitilazed name of the app: " + " $appCaps");
}
