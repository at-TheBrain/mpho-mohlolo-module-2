void main() {
  String name = "Mpho";
  String favoriteApp = "Pulse Brightspace";
  String city = "Johanneburg";

  print("My name is: " +
      "$name" +
      "\nMy favorite App is: " +
      "$favoriteApp" +
      "\nAnd I am from: $city");
}

// data: Your name, favorite app, and city;

